
NOTE
----

This is an api module for developers. It doesnt do much on its own. Please install if required by other modules.

DESCRIPTION
-----------

og_settings module helps developers to use a standard api and database schema for getting/setting persistent variables 
for each organic group. The api (and implementation) is similar to drupal core api functions for storing/getting/setting 
site-wide persistent variables (variable_get(), variables_set(), variable_del()):

og_settings_variable_set($gid, $name, $value) : Sets a persistent variable for group "$gid"

og_settings_variable_get($gid, $name, $default) : Returns a persistent variable for the group "$gid"

og_settings_variable_del($gid, $name) : Delete a persistent variable "$name" for organic group "$gid"

og_settings_variable_del_all($gid) : Deletes all persistent variables for organic group "$gid"


This module also simplify the process of creating forms used by users (typically group administrators) to set/change 
the value of persistent variables for their group (similar to "system_setting_form()" offered by drupal core):

og_settings_form($form, $gid) : Add default buttons to an og settings form.

NOTE
----

Variable values are serialized and cached, so you are free to through anything in there.


EXAMPLE
-------

There is a mini module (og_info) included in this package as an example of how a module would use og_settings API. 

og_info allows group administrators to provide some information about their group (group name, mail, slogan, footer etc).
It's similar to how Drupal core (system module) allows site administrators to set some information about the web 
site (see admin/settings/site-information) 

Developers/themers can use this info in different ways. Here is what I do in one my sites: 

- Add the following code in the "template.php" file:

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {

	switch($hook){
		case 'node':
      // you maybe have some code here
			return $vars;
			break;
			
		case 'page':

			$gnode = og_get_group_context();
			if ($gnode -> nid){
				$vars['site_name'] = og_settings_variable_get($gnode -> nid, 'og_info_name', '');
				$vars['site_mail'] = og_settings_variable_get($gnode -> nid, 'og_info_mail', '');
				$vars['site_slogan'] = og_settings_variable_get($gnode -> nid, 'og_info_slogan', '');
				$vars['site_mission'] = og_settings_variable_get($gnode -> nid, 'og_info_mision', '');
				$vars['site_footer'] = og_settings_variable_get($gnode -> nid, 'og_info_footer', '');
			return $vars;
			break;

		case 'comment':
			// maybe some more code
			return $vars;
			break;
	}

	return array();
}


- You usually dont have to change anything in page.tpl.php:

e.g. print $site_name will print  "og_info_name" if you are looking at a particluar group, or the Drupal "site_name" 
if in other part of your site.




